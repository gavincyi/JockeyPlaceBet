"""Jocky place bet.

Usage: jocky-place-bet --configuration <CONFIGURATION>

Options:
    -h                                  :Show help.
    --configuration=<CONFIGURATION>     :Configuraiton file path.
"""

import logging

from docopt import docopt
import yaml

from jockey import __version__
from jockey.database import Database
from jockey.browser import Browser
from jockey.scheduler import Scheduler


LOGGER = logging.getLogger(__name__)


def init_db(connection):
    """Initialize database.
    """
    LOGGER.info('Initializing the database')
    engine_type = connection.get('engine_type', 'mysql')
    host = connection['host']
    user = connection['user']
    password = connection['password']
    database = connection['database']

    return Database(
        engine_type=engine_type,
        host=host,
        user=user,
        password=password,
        database=database)


def get_sql_command(configuration):
    """Get SQL command.
    """
    if 'sql_command' not in configuration.keys():
        return None
    else:
        configuration = configuration['sql_command']
        if 'filename' not in configuration.keys():
            raise ValueError('File name is not provided')
        else:
            return open(configuration['filename']).read()


def create_scheduler(configuration_path):
    """Initialize.
    """
    configuration = open(configuration_path).read().replace('\t', ' ' * 4)
    configuration = yaml.load(configuration)
    database = init_db(configuration['database'])
    browser = Browser(**configuration['browser'])
    sql_command = get_sql_command(configuration)
    scheduler = Scheduler(
        database=database,
        browser=browser,
        sql_command=sql_command,
        **configuration.get('scheduler', {}))

    return scheduler


def main():
    """Jockey place bet.
    """
    args = docopt(__doc__, version=__version__)
    logging.basicConfig(
        level=logging.DEBUG,
        format='%(asctime)s %(levelname)s %(message)s')

    LOGGER.info('Loading configuration %s', args["--configuration"])
    scheduler = create_scheduler(configuration_path=args["--configuration"])

    LOGGER.info('Loading scheduler')
    scheduler.load()

    LOGGER.info('Running scheduler')
    scheduler.run()

    LOGGER.info('Finished placing all the bets')
    input('Press any key to exit')


if __name__ == '__main__':
    main()

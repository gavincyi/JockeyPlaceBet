import logging
import re

from .base_bettor import BaseBettor

LOGGER = logging.getLogger(__name__)


class DoubleTrio(BaseBettor):
    """Double trio.
    """
    def bet(self, bankers, legs):
        """Bet.
        """
        LOGGER.info('Bankers:\n%s', bankers)
        LOGGER.info('Legs:\n%s', legs)

        boxes = self._get_betting_box()

        for _, banker in bankers.iterrows():
            # Click banker
            self._click_button(
                boxes=boxes,
                row=banker,
                type_name='banker',
                button_index=1)

        for _, leg in legs.iterrows():
            # Click legs
            self._click_button(
                boxes=boxes,
                row=leg,
                type_name='leg',
                button_index=2)

    def _get_betting_box(self):
        """Get betting box.
        """
        main_boxes = self._browser.find_elements_by_class_name('twoXtwoDiv')
        if len(main_boxes) != 2:
            raise ValueError('Number of double main boxes (%d) is not 2' %
                             main_boxes)

        boxes = {}

        for index, box in enumerate(main_boxes):
            title = box.find_element_by_class_name('titleBar').text
            LOGGER.debug('Box %d text: %s', index, title)
            raceno = re.match(r'.*Race (\d+)', title)

            if raceno is None:
                raise RuntimeError(
                    'Cannot find race number on title %s' % title)

            raceno = int(raceno.groups()[0])
            box = box.find_element_by_id('wpTable%dInnerTable' % raceno)
            boxes[raceno] = box

        return boxes

    def _click_button(self, boxes, row, type_name, button_index):
        """Click button.
        """
        LOGGER.debug('Clicking on the row %s', row)
        raceno = row['race_num']
        leg = row['leg']
        horse_tds = boxes[raceno].find_elements_by_tag_name('tr')
        horse = horse_tds[leg].find_elements_by_tag_name('td')
        horse_name = horse[3].text
        LOGGER.debug(
            'Selecting %s on race %d at leg %d with name %s',
            type_name, raceno, leg, horse_name)
        button = horse[button_index].find_element_by_class_name(
            'checkbox')
        button.click()

import logging

LOGGER = logging.getLogger(__name__)


class BaseBettor:
    """Base bettor class.
    """
    def __init__(self, browser, unit_bet, raceno, dry_run=False):
        """Constructor.
        """
        self._browser = browser
        self._unit_bet = unit_bet
        self._raceno = raceno
        self._dry_run = dry_run

    def execute(self, **kwargs):
        """Execute.
        """
        self.bet(**kwargs)
        self._browser.place_unit_bet(unit_bet=self._unit_bet)

        if not self._dry_run:
            self._browser.add_to_slip()
        else:
            LOGGER.info('Running dry run mode on %s', self.__class__.__name__)

    def bet(self, **kwargs):
        """Bet.
        """
        raise NotImplementedError('Not implemented betting method')

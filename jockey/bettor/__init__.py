from .win_place import Win, Place
from .qin_qpl_qqp import Qin, Qpl, Qqp
from .double_trio import DoubleTrio
from .triple_trio import TripleTrio

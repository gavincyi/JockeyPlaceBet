import logging

from .base_bettor import BaseBettor

LOGGER = logging.getLogger(__name__)


class QinQplQqpBettor(BaseBettor):
    """Qin qpl bettor.
    """
    def bet(self, bet_type, leg1, leg2):
        """Bet.
        """
        LOGGER.info('Placing %s on race %d',
                    self.__class__.__name__,
                    self._raceno)
        self._browser.find_element_by_id('%sRadio' % bet_type).click()

        LOGGER.info('Placing %s on race %d on leg %d and %d',
                    self.__class__.__name__,
                    self._raceno,
                    leg1,
                    leg2)
        element = self._browser.find_element_by_id('wpTable1')

        for leg in [leg1, leg2]:
            elements = element.find_elements_by_xpath('div/table/tbody/tr')
            set_bet = elements[int(leg)].find_elements_by_xpath('td')[2]
            set_bet.click()


class Qin(QinQplQqpBettor):
    """Qin.
    """
    def bet(self, leg1, leg2):
        """Bet.
        """
        super().bet(bet_type='qin', leg1=leg1, leg2=leg2)


class Qpl(QinQplQqpBettor):
    """Qpl.
    """
    def bet(self, leg1, leg2):
        """Bet.
        """
        super().bet(bet_type='qpl', leg1=leg1, leg2=leg2)


class Qqp(QinQplQqpBettor):
    """Qin.
    """
    def bet(self, leg1, leg2):
        """Bet.
        """
        super().bet(bet_type='qqp', leg1=leg1, leg2=leg2)

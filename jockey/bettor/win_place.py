import logging

from .base_bettor import BaseBettor

LOGGER = logging.getLogger(__name__)


class Win(BaseBettor):
    """Win.
    """
    def bet(self, leg):
        """Execute.
        """
        LOGGER.info('%s bet on race %d leg %s',
                    self.__class__.__name__, self._raceno, leg)
        cell = self._browser.find_element_by_id('winCell%d' % leg)
        cell.find_element_by_class_name('checkbox').click()


class Place(BaseBettor):
    """Place.
    """
    def bet(self, leg):
        """Execute.
        """
        LOGGER.info('%s bet on race %d leg %s',
                    self.__class__.__name__, self._raceno, leg)
        cell = self._browser.find_element_by_id('plaCell%d' % leg)
        cell.find_element_by_class_name('checkbox').click()

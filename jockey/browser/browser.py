import logging

from selenium import webdriver

LOGGER = logging.getLogger(__name__)


class Browser(webdriver.Chrome):
    """Browser.
    """

    MENU = {
        'win_place': 2,
        'qin_qpl_qqp': 3,
        'double_trio': 14,
        'triple_trio': 15,
    }

    def __init__(self, **kwargs):
        """Constructor.
        """
        super().__init__(**kwargs)
        self._is_login = False

    def load_default(self):
        """Load default page.
        """
        self.get('http://bet.hkjc.com/racing/default.aspx?lang=en')

    def switch_bet_frame(self):
        """Switch bet frame.
        """
        LOGGER.debug('Switching to bet frame')
        self.switch_to.default_content()
        bet_frame = self.find_elements_by_tag_name('iframe')[0]
        self.switch_to_frame(bet_frame)
        LOGGER.debug('Switched to bet frame')

    def switch_credential_frame(self):
        """Switch credential frame.
        """
        LOGGER.debug('Switching to credential frame')
        self.switch_to.default_content()
        credential_frame = self.find_element_by_id('iframeDisplay')
        credential_frame = credential_frame.find_elements_by_tag_name(
            'iframe')[0]
        self.switch_to_frame(credential_frame)
        LOGGER.debug('Switched to credential frame')

    def go_menu(self, menu):
        """Go menu.
        """
        self.switch_bet_frame()
        menu_click = self.find_element_by_id('oddsMenu')
        menu_click = menu_click.find_elements_by_xpath('div/div')
        menu_click[self.MENU[menu]].find_elements_by_xpath('div')[1].click()

    def login(self, name, password):
        """Login.
        """
        self.switch_credential_frame()

        LOGGER.info('Logging in with name %s', name)
        name_input = self.find_element_by_id('account')
        name_input.clear()
        name_input.send_keys(name)

        try:
            password_input = self.find_element_by_id('passwordInput1')
            password_input.clear()
            password_input.send_keys(password)
        except:
            password_input = self.find_element_by_id('password')
            password_input.clear()
            password_input.send_keys(password)

        login_button = self.find_element_by_id(
            'divAccInfoDefaultLoginButton')
        login_button.click()

        try:
            self.find_element_by_id('errMsgTxt')
            LOGGER.info('Failed to login')
        except:
            # TODO: Should recognize the successful login by reliable messages
            LOGGER.info('Login successful')
            self._is_login = True

    def get_total_race_number(self):
        """Get total race number.
        """
        LOGGER.info('Getting total race number')

        self.load_default()
        # self.switch_bet_frame()

        length = len(self.find_elements_by_css_selector("div[class^='raceNo']"))

        LOGGER.info('Total race number: %d', length)

        return length

    def go_default(self):
        """Go to default page.
        """
        LOGGER.info('Going to default page')

        self.switch_bet_frame()
        self.find_element_by_id('secMenuHR').click()

    def go_raceno(self, raceno):
        """Go to race number.
        """
        LOGGER.info('Going to race number %d', raceno)

        self.switch_bet_frame()

        LOGGER.info('Gong to race number %d', raceno)
        race_buttons = self.find_elements_by_class_name('raceButton')
        race_buttons[raceno-1].click()

    def get_race_info(self, raceno):
        """Get race_info.
        """
        self.go_default()
        self.go_raceno(raceno=raceno)

        LOGGER.info('Getting race info %d', raceno)
        content = self.find_elements_by_class_name('content')[-1]
        infos = content.find_elements_by_tag_name('nobr')
        infos = {
            'date': infos[0].text,
            'time': infos[1].text,
        }

        LOGGER.info('Race info %d: %s', raceno, infos)

        return infos

    def add_to_slip(self):
        """Add to slip.
        """
        LOGGER.debug('Add to slip')
        self.switch_bet_frame()
        self.find_element_by_id('raceInfobtn_addslip').click()

    def place_unit_bet(self, unit_bet):
        """Place unit bet.
        """
        LOGGER.debug('Place unit bet %d', unit_bet)
        self.switch_bet_frame()
        element = self.find_element_by_id('betAmountCalTopUnitBetInput')
        element.clear()
        element.send_keys('%d' % unit_bet)

    def refresh_balance(self):
        """Refresh balance.
        """
        LOGGER.debug('Refresh balance')
        self.switch_credential_frame()
        self.find_element_by_id('refresh_balance').click()

    def reset(self):
        """Reset
        """
        self._is_login = False

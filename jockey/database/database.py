import logging

from pandas import read_sql, to_datetime
from sqlalchemy import create_engine

LOGGER = logging.getLogger(__name__)

class Database:
    """Database instance.
    """

    AUTOIT_ALL_TABLE = '_autoit_all'
    AUTOIT_TRIPLE_TRIO_BANKER = '_autoit_triple_trio_banker'
    AUTOIT_TRIPLE_TRIO_LEG = '_autoit_triple_trio_leg'
    AUTOIT_DOUBLE_TRIO_BANKER = '_autoit_double_trio_banker'
    AUTOIT_DOUBLE_TRIO_LEG = '_autoit_double_trio_leg'
    AUTOIT_BET_PARAM = '_auto_bet_param'

    def __init__(self, engine_type, host, user, password, database, port=3306):
        """Constructor.
        """
        self._engine_type = engine_type
        self._host = host
        self._user = user
        self._password = password
        self._database = database
        self._port = port
        self._engine = None

    @property
    def engine(self):
        """Engine.
        """
        if self._engine is None:
            self.load()

        return self._engine

    @property
    def auto_bet_param(self):
        """Autoit all table.
        """
        table = self.read_table(self.AUTOIT_BET_PARAM)
        table['race_date'] = table['race_date'].apply(
            lambda x: to_datetime(x, format="%Y%m%d"))
        return table.set_index('race_date')

    @property
    def autoit_all(self):
        """Autoit all table.
        """
        return self.read_table(self.AUTOIT_ALL_TABLE)

    @property
    def autoit_2t_banker(self):
        """Autoit double trio banker.
        """
        return self.read_table(self.AUTOIT_DOUBLE_TRIO_BANKER)

    @property
    def autoit_2t_leg(self):
        """Autoit double trio leg.
        """
        return self.read_table(self.AUTOIT_DOUBLE_TRIO_LEG)

    @property
    def autoit_3t_banker(self):
        """Autoit triple trio banker.
        """
        return self.read_table(self.AUTOIT_TRIPLE_TRIO_BANKER)

    @property
    def autoit_3t_leg(self):
        """Autoit triple trio leg.
        """
        return self.read_table(self.AUTOIT_TRIPLE_TRIO_LEG)

    def load(self):
        """Load.
        """
        LOGGER.info('Loading engine %s from %s:%s/%s with user %s',
                    self._engine_type,
                    self._host,
                    self._port,
                    self._database,
                    self._user)
        connection_string = (
                "{engine_type}://{user}:{pwd}@{host}:{port}/{db}".format(
                    engine_type=self._engine_type,
                    user=self._user,
                    pwd=self._password,
                    host=self._host,
                    port=self._port,
                    db=self._database))

        engine = create_engine(connection_string, echo=True)
        engine = engine.connect()
        self._engine = engine

    def read_table(self, table):
        """Read table.
        """
        sql = "select * from {}".format(table)
        data = read_sql(sql, con=self.engine)
        return data

    def execute(self, sql_command):
        """Execute SQL.
        """
        LOGGER.debug('Executing SQL command %s', sql_command[:100])
        connection = self._engine.connect()
        results = connection.execute(sql_command, multi=True)
        connection.close()

        LOGGER.debug('Execution result: %s', results)

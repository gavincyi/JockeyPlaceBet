import logging
from time import sleep

import pandas as pd

from jockey.bettor import (
    Win,
    Place,
    Qpl,
    Qin,
    Qqp,
    DoubleTrio,
    TripleTrio,
)

LOGGER = logging.getLogger(__name__)


class Scheduler:
    """Scheduler.
    """

    def __init__(self, browser, database, sql_command=None, max_wait_min=4,
                 dry_run=False):
        """Constructor.
        """
        self._browser = browser
        self._database = database
        self._sql_command = sql_command
        self._total_races = 0
        self._auto_bet_param = None
        self._racing_date = None
        self._minute_ahead = None
        self._race_info = pd.DataFrame()
        self._max_wait_min = max_wait_min
        self._dry_run = dry_run

    def load(self):
        """Load.
        """
        LOGGER.info('Loading time is %s', self._current_time())

        # Load the auto bet param. This is only loaded once and
        # used for the whole life
        LOGGER.info('Loading auto bet param')
        self._auto_bet_param = (
            self._database.auto_bet_param.sort_index())
        LOGGER.info('Auto bet param:\n%s', self._auto_bet_param)

        # The latest race date
        if len(self._auto_bet_param) == 0:
            raise RuntimeError('Empty auto bet param')

        self._racing_date = self._auto_bet_param.index[-1]
        LOGGER.info('Racing date: %s', self._racing_date)

        self._minute_ahead = (
            self._auto_bet_param.loc[self._racing_date]['minute_ahead'])
        LOGGER.info('Bet minute ahead: %s', self._minute_ahead)

        # Get the total race number. Also loaded once only
        LOGGER.info('Getting total races')
        self._total_races = self._browser.get_total_race_number()
        LOGGER.info('Total number of races: %d', self._total_races)

        # Show whether in dry run mode
        LOGGER.info('Dry run mode: %s', self._dry_run)

    def run(self):
        """Run.
        """
        try:
            for raceno in range(1, self._total_races + 1):
                LOGGER.info("Running race %d", raceno)

                while True:
                    wait_time = self.get_race_wait_time(raceno)

                    if wait_time == -1:
                        LOGGER.info('Race %d started racing already.',
                                    raceno)
                        break
                    elif wait_time == 0:
                        LOGGER.info('Race %d is available to place bets',
                                    raceno)
                        self.execute_command()
                        self.place_race_bets(raceno=raceno)
                        LOGGER.info('All the bets are placed on race %d', raceno)

                        if self._minute_ahead >= 0:
                            LOGGER.info(
                                'Sleep for %.1f minutes', min(5, self._max_wait_min))
                            sleep(min(5, self._max_wait_min) * 60.0 + 5.0)

                        break
                    else:
                        self.refresh_balance()

                        updated_wait_time = min(wait_time / 2, self._max_wait_min)
                        LOGGER.info(
                            'Race %d is still before betting time. Will '
                            'wait for %.1f (original %.1f) minutes',
                            raceno, updated_wait_time, wait_time)
                        sleep(updated_wait_time * 60.0)
        except:
            from datetime import datetime
            self._browser.save_screenshot(
                'error_screen_%s.png' % datetime.now().strftime('%Y%m%d%H%M%S'))
            raise

    def place_race_bets(self, raceno):
        """Get the race bets.
        """
        bets = self._database.autoit_all
        bets = bets[bets['race_num'] == raceno]

        LOGGER.info('Race %d bets:\n%s', raceno, bets)
        for _, row in bets.iterrows():
            self._place_bet(raceno, row)

    def get_race_wait_time(self, raceno):
        """Get race waiting time.

        :returns Integer indicating the minutes to wait.
            -1 means the race is started and no bets should be
            placed. 0 means the current time has passed the
            bet time.
        """
        # Update race info again
        race_time = self._update_race_time(raceno=raceno)

        # Betting time
        betting_time = (
            race_time -
            pd.Timedelta(minutes=self._minute_ahead))
        LOGGER.info('Betting time of race %d: %s', raceno, betting_time)

        # Check whether to wait
        current_time = self._current_time()
        if current_time >= race_time:
            LOGGER.info(
                'Current time %s has passed the race %d time %s',
                current_time, raceno, race_time)
            return -1
        elif current_time >= betting_time or self._minute_ahead <= 0:
            LOGGER.info(
                'Current time %s has passed the betting time %s '
                '(race %d time %s with %d minute(s) ahead)',
                    current_time, betting_time, raceno,
                    race_time, self._minute_ahead)
            return 0
        else:
            minutes_diff = (betting_time - current_time).total_seconds() / 60
            LOGGER.info(
                'Current time %s is still %.2f minutes before the betting '
                'time %s (race %d time %s with %d minute(s) ahead)',
                    current_time, minutes_diff,
                    betting_time, raceno, race_time, self._minute_ahead)
            return minutes_diff

    def _current_time(self):
        """Get current time.
        """
        current_time = pd.Timestamp.now()
        LOGGER.debug('Current time is %s', current_time)

        return current_time

    def _get_race_time(self, raceno):
        """Get race time.
        """
        LOGGER.debug('Get race %d time', raceno)
        race_info = self._browser.get_race_info(raceno=raceno)
        time_string = race_info['date'] + ' ' + race_info['time']
        time = pd.to_datetime(time_string, format='%d/%m/%Y %H:%M')

        if time.date() != self._racing_date.date():
            raise ValueError(
                'Default race date %s is different from database '
                'race date %s. Please check the table auto_bet_param',
                    self._racing_date, time)

        LOGGER.debug('Race %d time: %s', raceno, time)

        return time

    def _update_race_time(self, raceno):
        """Update race time.
        """
        LOGGER.info('Updating race %d time', raceno)
        self._race_info.loc[raceno, 'time'] = (
            self._get_race_time(raceno=raceno))

        return self._race_info.loc[raceno, 'time']

    def _place_bet(self, raceno, row):
        """Place bet.
        """
        LOGGER.info('Placing race %d on row %s', raceno, row)

        bet_type = row['bet_type']
        unit_bet = row['unit_bet']
        kwargs = {
            'browser': self._browser,
            'unit_bet': unit_bet,
            'raceno': raceno,
            'dry_run': self._dry_run,
        }

        if bet_type == 'place':
            self._browser.go_menu('win_place')
            bet = Place(**kwargs)
            bet.execute(leg=row['leg1'])
        elif bet_type == 'win':
            self._browser.go_menu('win_place')
            bet = Win(**kwargs)
            bet.execute(leg=row['leg1'])
        elif bet_type == 'qin':
            self._browser.go_menu('qin_qpl_qqp')
            bet = Qin(**kwargs)
            bet.execute(leg1=row['leg1'], leg2=row['leg2'])
        elif bet_type == 'qpl':
            self._browser.go_menu('qin_qpl_qqp')
            bet = Qpl(**kwargs)
            bet.execute(leg1=row['leg1'], leg2=row['leg2'])
        elif bet_type == 'qqp':
            self._browser.go_menu('qin_qpl_qqp')
            bet = Qqp(**kwargs)
            bet.execute(leg1=row['leg1'], leg2=row['leg2'])
        elif bet_type == 'double_trio':
            self._browser.go_menu('double_trio')
            bet = DoubleTrio(**kwargs)
            bet.execute(
                bankers=self._database.autoit_2t_banker,
                legs=self._database.autoit_2t_leg)
        elif bet_type == 'triple_trio':
            self._browser.go_menu('triple_trio')
            bet = TripleTrio(**kwargs)
            bet.execute(
                bankers=self._database.autoit_3t_banker,
                legs=self._database.autoit_3t_leg)
        else:
            raise NotImplementedError(
                'Cannot recognize type %s on row %s' % (
                    bet_type, row))

    def refresh_balance(self):
        """Refresh balance.
        """
        try:
            self._browser.refresh_balance()
        except:
            LOGGER.info('Account is not logged in. '
                        'Cannot refresh the balance.')

    def close(self):
        """Close.
        """
        self._browser.close()

    def execute_command(self):
        """Execute command.
        """
        if self._sql_command is None:
            LOGGER.debug('No SQL command to execute')
        else:
            LOGGER.debug('Execute command')
            self._database.execute(self._sql_command)

#!/bin/bash

jockey-place-bet --configuration configuration_simple.yaml 2>&1 | tee configuration_simple.yaml.out
jockey-place-bet --configuration configuration_double_trio.yaml 2>&1 | tee configuration_double_trio.yaml.out
jockey-place-bet --configuration configuration_triple_trio.yaml 2>&1 | tee configuration_triple_trio.yaml.out

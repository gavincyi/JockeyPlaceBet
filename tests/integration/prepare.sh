#!/bin/bash

DRY_RUN=$1

echo "Dry run = $DRY_RUN"

if [ "$DRY_RUN" == "0" ]; then
    sed -i 's/dry_run: .*/dry_run: False/g' *.yaml
else
    sed -i 's/dry_run: .*/dry_run: True/g' *.yaml
fi

jockey-place-bet --configuration configuration_prepare.yaml

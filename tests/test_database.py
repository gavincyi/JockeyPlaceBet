import pytest

from jockey.database import Database


@pytest.fixture(scope="module")
def database(database_connection):
    """Database.
    """
    database = Database(
        host=database_connection['host'],
        database=database_connection['database'],
        user=database_connection['user'],
        password=database_connection['password']
    )

    return database


def test_auto_bet_param(database):
    """Test autoit all table.
    """
    assert database.auto_bet_param is not None


def test_autoit_all(database):
    """Test autoit all table.
    """
    assert database.autoit_all is not None


def test_autoit_3t_banker(database):
    """Test autoit 3T banker table.
    """
    assert database.autoit_3t_banker is not None

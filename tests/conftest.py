import pytest


@pytest.fixture(scope="session")
def database_connection():
    """Database connection.
    """
    return {
        # 'host': 'horserace.c35lrzl50v5k.us-west-2.rds.amazonaws.com',
        'host': '165.227.10.51',
        'database': 'horserace-place',
        'user': 'horserace',
        'password': 'horserace',
    }

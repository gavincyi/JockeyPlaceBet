==============
JockeyPlaceBet
==============


.. image:: https://img.shields.io/pypi/v/JockeyPlaceBet.svg
        :target: https://pypi.python.org/pypi/JockeyPlaceBet

.. image:: https://img.shields.io/travis/audreyr/JockeyPlaceBet.svg
        :target: https://travis-ci.org/audreyr/JockeyPlaceBet

.. image:: https://readthedocs.org/projects/JockeyPlaceBet/badge/?version=latest
        :target: https://JockeyPlaceBet.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status

.. image:: https://pyup.io/repos/github/audreyr/JockeyPlaceBet/shield.svg
     :target: https://pyup.io/repos/github/audreyr/JockeyPlaceBet/
     :alt: Updates


Jockey club bet placement application.


* Free software: Apache Software License 2.0
* Documentation: https://JockeyPlaceBet.readthedocs.io.


Features
--------

* TODO

Credits
---------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage


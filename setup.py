#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = [
    'beautifulsoup4>=4.6.1',
    'docopt>=0.6.2',
    'pandas>=0.23.0'
    'PyMySQL>=0.7.11',
    'pyyaml>=3.12',
    'SQLAlchemy>=1.1.15',
    'selenium>=3.14.0',
]

setup_requirements = [
    'pytest-runner',
]

test_requirements = [
    'pytest',
]

setup(
    name='JockeyPlaceBet',
    version='0.1.0',
    description="Jockey club bet placement application.",
    long_description=readme + '\n\n' + history,
    author="Jack Ho",
    author_email='aroy@alum.mit.edu',
    # url='https://github.com/audreyr/JockeyPlaceBet',
    packages=find_packages(include=['jockey']),
    include_package_data=True,
    install_requires=requirements,
    license="Apache Software License 2.0",
    zip_safe=False,
    keywords='JockeyPlaceBet',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: Apache Software License',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ],
    test_suite='tests',
    tests_require=test_requirements,
    setup_requires=setup_requirements,
    entry_points="""
        [console_scripts]
        jockey-place-bet = jockey.cli.jockey_place_bet:main
    """,
)
